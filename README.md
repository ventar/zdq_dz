
## 张大千定制页面

***

#### 一、Banner图片对应栏目

1. **生平与著述** ![banner_1.png](https://bytebucket.org/ventar/zdq_dz/raw/21fd5e408b7e271fa9f053056b3c114b2c4048f3/assets/image/banner_1.png)
2. **大风堂画派** ![banner_3.png](https://bytebucket.org/ventar/zdq_dz/raw/21fd5e408b7e271fa9f053056b3c114b2c4048f3/assets/image/banner_3.png)
3. **研究文献** ![banner_2.png](https://bytebucket.org/ventar/zdq_dz/raw/21fd5e408b7e271fa9f053056b3c114b2c4048f3/assets/image/banner_2.png)
4. **研究者与机构** ![banner_4.png](https://bytebucket.org/ventar/zdq_dz/raw/21fd5e408b7e271fa9f053056b3c114b2c4048f3/assets/image/banner_4.png)

***
#### 二、栏目详细

1. **张大千生平与著述**文件夹：1
> - *综述* `详情`[about]
> - *个人著述*
> > - *日记* `图片列表`[diary]
> > - *手稿* `图片列表`[manuscript]
> > - *诗文集* `文字列表`[poem]
> > - *印集* `图片列表`[seal]
> > - *家书* `文字列表`[letter]
> - *生平事迹*
> > - *生平* `详情`[lifetime]
> > - *回忆* `文字列表`[memory]
> > - *趣闻轶事* `文字列表`[anecdote]
> - *交游* `人物列表`[people]
> - *年谱* `详情`[chronicle]

2. **绘画成就**文件夹：2
> - *画集画册*
> - *重要作品*

3. **大风堂画派**文件夹：3
> - *综述* `详情`[about]
> - *代表人物* `人物列表`[people]

4. **研究文献**文件夹：4
> - *图书*
> - *报刊论文*
> - *学位论文*
> - *网络导航*
> - *参考工具书与检索工具*

5. **研究者与研究机构**文件夹：5
> - *研究者*
> - *研究机构*