/**
 * Created by zhxfj on 2017/11/27.
 */
// 用于设置中间内容的最小高度
function mainMinH() {
  var headerH = $('header').outerHeight();
  var footer = $('footer').outerHeight();
  var windowH = $(window).height();
  var minH = windowH - headerH - footer;
  $(".main").css("min-height", minH);
}

//  改变窗口大小  中间内容高度更改
function resize() {
  $(window).resize(function () {
    mainMinH();
  })
}

//  左侧目录展开，收起
function navFold(func) {
  //一级目录的点击
  $(".navs>li>div").off("click").on("click", function (e) {
    var _this = $(this).parent();
    var thisHref=$(this).children("a").attr("href");
    var ulLi = $(_this.children("ul")[0]);
    e.preventDefault();
    if(ulLi.length>0){
      if (_this.hasClass("fold")) {
        _this.children("ul").css("display","none");
        _this.removeClass("fold current");
        $(this).find(".icon-img").attr("src", "../assets/image/icon_7.png");
      }else{
        _this.children("ul").css("display","block");
        _this.addClass("fold ");
        if(_this.find(".current").length==0){
          $(".navs .current").removeClass("current");
          _this.addClass("current");
          ulLi.children("li").eq(0).children("a").click();
        }
        $(this).find(".icon-img").attr("src", "../assets/image/icon_1.png");
      }
    }else{
      $(".navs .current").removeClass("current");
      _this.addClass("current");
      thisHref&&func&&typeof(func)=="function"&&func(thisHref);
    }
  });
  //二级目录的点击
  $(".sub-nav>li>a").off("click").on("click",function (e) {
    e.preventDefault();
    var _this=$(this).parent();
    var thisHref=$(this).attr("href");
    if(_this.hasClass('current')){
      if(_this.children("ul").length>0){
        if(_this.hasClass("fold")){
          _this.removeClass("fold");
          _this.children("ul").css("display","none");
          $(this).find(".icon").attr("src", "../assets/image/icon_9.png");
        }else{
          _this.addClass("fold");
          _this.children("ul").css("display","block");
          $(this).find(".icon").attr("src", "../assets/image/icon_8.png");
        }
      }
    }else{
      $(".navs .current").removeClass("current");
      _this.addClass("current");
      $(this).parents("li").siblings().removeClass('current');
      if(_this.children("ul").length>0){
        _this.addClass("fold");
        _this.children("ul").css("display","block");
        $(this).find(".icon").attr("src", "../assets/image/icon_8.png");
      }
      thisHref&&func&&typeof(func)=="function"&&func(thisHref);
    }

  });
//  锚链接的链接
  $(".mao>li").on("click",function (e) {
    e.preventDefault();
    var thisHref=$(this).find("a").attr("href");
    if(!$(this).hasClass("current")){
      $(".navs .current").removeClass("current");
      $(this).addClass("current");
    }
    thisHref&&func&&typeof(func)=="function"&&func(thisHref);
  })
}

// 左侧目录的展开，收起  定位到最下面的一级
function navFold1(func) {
  $(".navs li").off("click").on("click", function (e) {
    e.stopPropagation();
    e.preventDefault();
    var thisHref=$(this).find("a").attr("href");
    var ulLi = $($(this).children("ul")[0]);
    var _this = $(this);
    var len = ulLi.length;
    if (len > 0) {
      if (_this.hasClass("fold")) {
        _this.find("ul").css("display","none");
        _this.removeClass("fold");
        ulLi.css("display", "none");
        if(_this.parent().hasClass("navs")){
          $(this).find(".icon-img").attr("src", "../assets/image/icon_7.png");
        }else if(_this.parent().hasClass("sub-nav")){
          $(this).find(".icon").attr("src", "../assets/image/icon_9.png");
        }
      } else {
        _this.addClass("fold");
        ulLi.css("display", "block");
        if(_this.parent().hasClass("navs")){
          $(this).find(".icon-img").attr("src", "../assets/image/icon_1.png");
          ulLi.find(".fold").removeClass("fold");
        }else if(_this.parent().hasClass("sub-nav")){
          $(this).find(".icon").attr("src", "../assets/image/icon_8.png");
        }
        $(".navs .current").removeClass("current");
        ulLi.find("li").eq(0).click();
      }
    } else {
      $(".navs .current").removeClass("current");
      _this.addClass("current");
    }
    thisHref&&func&&typeof(func)=="function"&&func(thisHref);
  })
}